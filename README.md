<img src="https://gitlab.com/MouleAGauffre/mandelbrot-with-quarkus/-/raw/master/examples/mandelbrot_16k.png" height="600px" alt=""/>

# mandelbrot-with-quarkus project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Run the container
The image is available here: https://hub.docker.com/repository/docker/pressepuree/mandelbrot-native

It's a native image using an executable built in a GraalVM container.
```shell script
docker run -p "8080:8080" --name "mandelbrot-server" pressepuree/mandelbrot-native
```

An alternative image - running the jar with a jdk 11 - is available here: https://hub.docker.com/repository/docker/pressepuree/mandelbrot
```shell script
docker run -p "8080:8080" --name "mandelbrot-server" pressepuree/mandelbrot
```

## Native vs JVM
For: http://localhost:8080/mandelbrot/1920/1080/1000

Now let's talk about performances. These are some measures i get on my pc (6 cores, 12 threads @4.7Ghz). Thanks to GraalVM bytecode to native code compilation, the elimination of the jit compilation the pruning of all the jvm dead code we can get some awesome performances boosts.

Image | jvm | native |
--- | --- | --- |
computation time (s) | 1.7 | 0.6 |
container size (Mo) | 149 | 62 |
startup time (s) | 0.7 | 0.014 |

As we can see there are some clear benefits of using GraalVM especially in microservices or serveless deployments !

## Try it
A full hd mandelbrot view:
http://localhost:8080/mandelbrot/1920/1080/1000

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/code-with-quarkus-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html