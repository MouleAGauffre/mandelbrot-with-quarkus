package org.acme

import com.mortennobel.imagescaling.experimental.ImprovedMultistepRescaleOp
import io.quarkus.arc.log.LoggerName
import java.awt.Color
import java.awt.image.BufferedImage
import java.util.stream.Stream
import javax.enterprise.context.ApplicationScoped
import org.apache.commons.math3.complex.Complex
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Timed
import org.jboss.logging.Logger
import kotlin.math.ceil
import kotlin.math.sqrt


@ApplicationScoped
class MandelbrotService {

    private val scaleFactor = System.getenv("SCALE_FACTOR").toFloat()

    @LoggerName("mandelbrot_logger")
    private lateinit var log: Logger

    @Timed(
        name = "MANDELBROT_COMP_TIME",
        description = "Mandelbrot set computation time",
        unit = MetricUnits.MILLISECONDS
    )
    fun compute(width: Int, height: Int, maxIt: Int, x: Int, y: Int, zoom: Int) =
        log.info(
            "Rendering mandelbrot set:\n - width: $width\n - height: $height\n - maxIt: $maxIt\n- n threads:  ${
                Runtime.getRuntime().availableProcessors().toDouble()
            }"
        ).let {
            val start = System.currentTimeMillis()
            val palette: Map<Int, IntArray> = buildPalette(maxIt)
            val semiWidth = (width * scaleFactor).toInt()
            val semiHeight = (height * scaleFactor).toInt()
            val frameBuffer = BufferedImage(semiWidth, semiHeight, BufferedImage.TYPE_INT_RGB)

            generateCSet(semiWidth, semiHeight, x, y, zoom)
                .map { computeMandelbrotPixel(it, maxIt) }
                .forEach { frameBuffer.raster.setPixel(it.x, it.y, palette[it.convergencePoint]) }

            log.info("Mandelbrot rendering completed in ${System.currentTimeMillis() - start} ms.\n\n")
            superResolution(frameBuffer, scaleFactor)
        }

    private fun superResolution(img: BufferedImage, scale: Float) =
        Pair((img.width / scale).toInt(), (img.height / scale).toInt()).let { dim ->
            BufferedImage(dim.first, dim.second, BufferedImage.TYPE_INT_RGB).apply {
                ImprovedMultistepRescaleOp(dim.first, dim.second).filter(img, this)
            }
        }

    private fun buildPalette(maxIt: Int) =
        (1f / maxIt).let { colorFactor -> IntRange(0, maxIt).associateWith { hsv2RGBArray(it * colorFactor) } }

    private fun hsv2RGBArray(it: Float) =
        Color.getHSBColor(0.4f, 0.3f, it * 0.9f).let { arrayOf(it.red, it.green, it.blue).toIntArray() }

    private fun generateCSet(width: Int, height: Int, x_: Int, y_: Int, zoom: Int): Stream<MandelbrotPoint> {
        val availableThreadsSqrt = ceil(sqrt(Runtime.getRuntime().availableProcessors().toDouble())).toInt()
        val pixelWidth: Double = (3.0 / zoom) / (width - 1.0)
        val pixelHeight: Double = (2.0 / zoom) / (height - 1.0)
        val chunkWidth = width / availableThreadsSqrt
        val chunkHeight = height / availableThreadsSqrt
        val centerX = x_ * pixelWidth
        val centerY = y_ * pixelHeight
        val minR = centerX - (2.0 / zoom)
        val minI = centerY - (1.0 / zoom)

        return (0 until availableThreadsSqrt).flatMap { i ->
            (i * chunkWidth).let {
                (0 until availableThreadsSqrt).map { j -> CSetChunk(it, j * chunkHeight, chunkWidth, chunkHeight) }
            }
        }
            .parallelStream()
            .flatMap { generateCSetChunk(it, pixelWidth, pixelHeight, minR, minI) }
    }

    private fun generateCSetChunk(
        chunk: CSetChunk, pixelWidth: Double, pixelHeight: Double, minR: Double, minI: Double
    ) =
        (chunk.x until chunk.x + chunk.width).flatMap { x ->
            (minR + x * pixelWidth).let { real ->
                (chunk.y until chunk.y + chunk.height).map { y ->
                    MandelbrotPoint(Complex(real, minI + y * pixelHeight), x, y)
                }
            }
        }.parallelStream()

    private fun computeMandelbrotPixel(c: MandelbrotPoint, maxIt: Int): Pixel {
        val y2 = c.y * c.y
        val xp1 = c.x + 1
        if (xp1 * xp1 + y2 < 0.0625) return Pixel(c.x, c.y, maxIt)      // optimization: check if in the core

        val p_ = c.x - 0.25
        val p = sqrt(p_ * p_ + y2)
        if (c.x < p - 2 * p * p + 0.25) return Pixel(c.x, c.y, maxIt)   // optimization: check if in the cardiod

        var zn = Complex(0.0, 0.0)
        for (iter in 0..maxIt) {
            val r2 = zn.real * zn.real
            val i2 = zn.imaginary * zn.imaginary

            if (r2 + i2 > 4.0) return Pixel(c.x, c.y, iter)

            zn = Complex(r2 - i2 + c.z.real, 2.0 * zn.real * zn.imaginary + c.z.imaginary)
        }

        return Pixel(c.x, c.y, maxIt)
    }

    private data class MandelbrotPoint(val z: Complex, val x: Int, val y: Int)
    private data class Pixel(val x: Int, val y: Int, val convergencePoint: Int)
    private data class CSetChunk(val x: Int, val y: Int, val width: Int, val height: Int)

}