package org.acme

data class RenderingQuery(val width: Int, val height: Int, val maxIt: Int, val x: Int, val y: Int, val zoom: Int)