package org.acme

import io.smallrye.mutiny.Uni
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import org.apache.commons.imaging.ImageFormats
import org.apache.commons.imaging.Imaging


@Path("/")
class MandelbrotResource {

    @Inject
    lateinit var mandelbrotService: MandelbrotService

    @POST
    @Path("/mandelbrot")
    @Produces("image/png")
    @Consumes("application/json")
    fun compute(renderingQuery: RenderingQuery): Uni<ByteArray> =
        Uni.createFrom().item {
            Imaging.writeImageToBytes(
                mandelbrotService.compute(
                    renderingQuery.width,
                    renderingQuery.height,
                    renderingQuery.maxIt,
                    renderingQuery.x,
                    renderingQuery.y,
                    renderingQuery.zoom
                ),
                ImageFormats.PNG,
                hashMapOf()
            )
        }

}