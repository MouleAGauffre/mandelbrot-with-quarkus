package org.acme

import io.quarkus.arc.log.LoggerName
import java.awt.image.BufferedImage
import org.jboss.logging.Logger

object Utils {

    @LoggerName("utils_logger")
    private lateinit var log: Logger

    fun compareImages(img1: BufferedImage, img2: BufferedImage) =
        if (img1.width != img2.width || img1.height != img2.height) {
            log.error("Both images should have same dimensions")
            false
        } else
            fetchImageData(img1).equals(fetchImageData(img2))

    private fun fetchImageData(img: BufferedImage) = img.raster.dataBuffer

}