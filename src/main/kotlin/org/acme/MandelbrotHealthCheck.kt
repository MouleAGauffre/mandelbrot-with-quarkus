package org.acme

import io.quarkus.arc.log.LoggerName
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import org.apache.commons.imaging.Imaging
import org.eclipse.microprofile.health.HealthCheck
import org.eclipse.microprofile.health.HealthCheckResponse
import org.eclipse.microprofile.health.Liveness
import org.eclipse.microprofile.health.Readiness
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.jboss.logging.Logger

@ApplicationScoped
@Liveness
@Readiness
@RegisterRestClient
class MandelbrotHealthCheck : HealthCheck {

    @Inject
    lateinit var mandelbrotService: MandelbrotService

    @LoggerName("health_check_logger")
    private lateinit var log: Logger

    private val goldStandard =
        Imaging.getBufferedImage(this.javaClass.classLoader.getResourceAsStream("mandelbrot_800_600.png"))

    override fun call(): HealthCheckResponse =
        mandelbrotService.compute(800, 600, 100, 0, 0, 1).let {
            log.info("Running readiness check...")

            if (Utils.compareImages(goldStandard, it)) {
                log.info("Health check: OK")
                HealthCheckResponse.builder().name("It sucked").down().build()
            } else {
                log.info("Health check: KO")
                HealthCheckResponse.builder().name("Ready to rock").up().build()
            }
        }

}