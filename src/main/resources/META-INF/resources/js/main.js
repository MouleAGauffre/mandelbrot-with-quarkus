let semiWidth = Math.ceil(window.innerWidth / 2)
let semiHeight = Math.ceil(window.innerHeight / 2)
let screen = document.getElementById("screen")
let host = "http://localhost:8080"
let headers = new Headers()
headers.append("Content-Type", "application/json")

let query = { width: window.innerWidth, height: window.innerHeight, maxIt: 200, x: 0, y: 0, zoom: 1 }

function render(query) {
    fetch(host + "/mandelbrot/compute", {
        method: "POST",
        body: JSON.stringify(query),
        headers: headers
    })
        .then(res => res.blob())
        .then(blob => screen.setAttribute('src', URL.createObjectURL(blob)))
        .catch(err => console.error(err))
}

$(document).ready(() => render(query))

screen.onclick = (e) => {
    query.zoom++
    query.maxIt+=10
    query.x = e.pageX - semiWidth
    query.y = e.pageY - semiHeight
    console.log(JSON.stringify(query))
    render(query)
}